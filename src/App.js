import './App.css';
import { StopwatchHolder } from './components/StopwatchHolder';

function App() {
  return (
    <div className="App">
      <div className="container">
        <StopwatchHolder />
      </div>
    </div>
  );
}

export default App;
