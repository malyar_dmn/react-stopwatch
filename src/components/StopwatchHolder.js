import React from 'react'
import { StopwatchButtonHolder } from './StopwatchButtonHolder'
import { StopwatchDisplay } from './StopwatchDisplay'

export const StopwatchHolder = () => {
    return (
        <div className="stopwatch-holder">
            <StopwatchDisplay />
            <StopwatchButtonHolder />
        </div>
    )
}
