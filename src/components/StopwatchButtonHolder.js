import React, { useContext } from 'react'
import { Context } from '../context'

export const StopwatchButtonHolder = () => {
    const {start, wait, reset} = useContext(Context)
    
    return (
        <div className="stopwatch-button-holder">
            <button onClick={start}>Start/Stop</button>
            <button onDoubleClick={wait}>Wait</button>
            <button onClick={reset}>Reset</button>
        </div>
    )
}
