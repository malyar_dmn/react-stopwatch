import React, {useEffect, useState} from 'react'
import { filter, interval } from 'rxjs'


const Context = React.createContext()

const ContextProvider = (props) => {
    const [time, setTime] = useState(0)
    const [isStarted, setIsStarted] = useState(false)
    const [status, setStatus] = useState('')

    useEffect(() => {
        const interval$ = interval(10)
            .pipe(
                filter(() => isStarted)
            )
            .subscribe(() => setTime(prev => prev + 1))
        
        return () => {
            interval$.unsubscribe()
        }
    }, [isStarted])

    
    const start = () => {
        if (status === 'waiting') {
            setIsStarted(prev => !prev)
            setStatus('started')
            return
        }
        if (status === 'started') {
            stop()
            return
        }

        setIsStarted(prev => !prev)
        setStatus('started')
    }

    const wait = () => {
        if (time !== 0) {
            setIsStarted(false)
            setStatus('waiting')
        }
    }

    const stop = () => {
        setTime(0)
        setIsStarted(prev => !prev)
        setStatus('stopped')
    }

    const reset = () => {
        setTime(0)
    }


    return (
        <Context.Provider value={{time, setTime, start, wait, reset}}>
            {props.children}
        </Context.Provider>
    )
}

export {Context, ContextProvider}